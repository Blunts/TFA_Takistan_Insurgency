class acs_mod
{
	tag = "ACS";

	class alive
	{
		file = "acs\functions\alive";
		class addAceTalk;
		class civTopicSetup;
		class reduceHostility;
	};

	class dialog
	{
		file = "acs\functions\dialog";
		class openDialog;
		class exitDialog;
		class displayBody;
		class preOpenDialog;
		class setupTopics;
		class topicSelected;
	};

	class playerAction
	{
		file = "acs\functions\playerAction";
		class getDown;
	};



};