params [["_man", objNull]];

if (isNull _man) exitWith {};

myaction = ["sc_talk"," Talk","\a3\Ui_f\data\IGUI\Cfg\Actions\talk_ca.paa",{[_this select 0] spawn ACS_fnc_preOpenDialog;},{alive(_this select 0) && !((_this select 0) getVariable ["ACE_isUnconscious", false])},{}] call ace_interact_menu_fnc_createAction;

[_man, 0, ["ACE_MainActions"], myaction] call ace_interact_menu_fnc_addActionToObject;

if (isServer) then
{
	_man spawn
	{
		sleep 0.5;
		_civ = _this;
		_civID = _civ getVariable ["agentID", ""];

		if (_civID != "") then
		{
			[_civ] call ACS_fnc_civTopicSetup;
		};
	};
};

if (hasInterface) then
{
	_man spawn
	{
		sleep 0.5;
		if (isPlayer _this) then
		{
			[_this, 0, ["ACE_MainActions", "sc_talk"]] call ace_interact_menu_fnc_removeActionFromObject;

			if(_this == player) then
			{

				orderAction = ['TA_OrderAction','Order Stop','scripts\iconOrder.paa',
				{
					_caller = player;

					[_caller, "GestureGo"] call ace_common_fnc_doGesture;

					_sendRadius = 15;

					_chance = 0.8;

					if (count weapons _caller == 0) then
					{
						_chance = 0;
					};

					{
					    if (count weapons _x == 0 && {random 1 < _chance}) then {

					        _x spawn
					        {
				        		_distance = random 30;
				        		_position = getPosASL player vectorAdd (eyeDirection player vectorMultiply (_distance + 1));

					        	doStop _this;

					        	sleep 20;
					        	_this doMove _position;
					        };
					    };
					    false
					} count (_caller nearEntities ["Civilian", _sendRadius]);


				},{!visibleMap}] call ace_interact_menu_fnc_createAction;
				[player, 1, ["ACE_SelfActions"], orderAction] call ace_interact_menu_fnc_addActionToObject;
			};
		}
		else
		{
			removeAllActions _this;
		};
	};
};

