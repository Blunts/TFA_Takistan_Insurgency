params ["_caller"];


[_caller, "GestureGo"] call ace_common_fnc_doGesture;

_sendRadius = 15;

_chance = 0.8;

if (count weapons _caller == 0) then
{
	_chance = 0;
};

{
    if (count weapons _x == 0 && {random 1 < _chance}) then {
        _x setUnitPos "DOWN";
        doStop _x;

        _x spawn
        {
        	sleep 20;
        	_position = getPos _this;
        	_this doMove _position;
        };
    };
    false
} count (_caller nearEntities ["Civilian", _sendRadius]);