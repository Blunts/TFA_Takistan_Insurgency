//Sanity check
#ifndef execNow
#define execNow call compile preprocessfilelinenumbers
#endif
//TFA OS
MISSION_ROOT = call { 
private "_arr"; 
_arr = toArray __FILE__; 
_arr resize (count _arr - 8); 
toString _arr 
};
//Load the altered StaticData
//call compile (preprocessFileLineNumbers "staticData.sqf");
//LT distance
LT_distance = 10; 
//Disable Saving
enableSaving [false, false];
//Enable VCOMAI
[] execVM "scripts\VCOMAI\init.sqf";
//Preload the arsenal
//["Preload"] call BIS_fnc_arsenal;
//TFAR INIT
tf_no_auto_long_range_radio = true;
tf_give_personal_radio_to_regular_soldier = true;
tf_give_microdagr_to_soldier = true;
tf_same_sw_frequencies_for_side = true;
tf_same_lr_frequencies_for_side = true;
tf_same_dd_frequencies_for_side = true;
//Personalized intro
if (isPlayer player && !isDedicated) then
{
waituntil {!(IsNull (findDisplay 46))};

   [    [      [format 
["%1",name player],"<t align = 'center' shadow = '1' size = 
'2'>%1</t><br/><br/>"],      
["WELCOME TO","<t align = 'center' shadow = '1' size = '1' 
font='TahomaB'>%1</t><br/>"],      
["Takistan Insurgency","<t align = 'center' shadow = '1' size = '1' 
font='PuristaBold'>%1</t><br/>"],      
["A [TFA] exclusive release.","<t align = 'center' shadow = '1' size = '0.5' 
font='TahomaB'>%1</t><br/><br/><br/>",20],      
["Just another day in Takistan.","<t align = 'center' shadow = '1' size = 
'0.5'>%1</t>",70]    ], 0, 0, "<t align = 'center' 
shadow = '1' size = '1.0'>%1</t>" ] spawn 
BIS_fnc_typeText;   sleep 10;
}; 
//StaticData loading
if (isServer) then {
 
//run on dedicated server or player host
 
    // override default data
    // see staticData.sqf
    ["MISSION INIT - Waiting"] call ALIVE_fnc_dump;
 
    waitUntil {!isNil "ALiVE_STATIC_DATA_LOADED"};
 
    ["MISSION INIT - Continue"] call ALIVE_fnc_dump;
 
    // override static data settings
    call compile preprocessFileLineNumbers "staticData.sqf";
 
    ["MISSION INIT - Static data override loaded"] call ALIVE_fnc_dump;
};
diag_log format["Initialisation Completed"];
	
