//Preload the Arsenal on clients
["Preload"] spawn BIS_fnc_arsenal;
//Add massive player rating to keep vehicles enterable with accidental TK's
player addRating 1000000;
//Add TFA OS to ACE/Equipment
/*
_action = ["open_tfaos", "TFA OS", "", {execVM "TFAOs\TFAOsMain.sqf";}, {true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "ACE_Equipment"], _action] call ace_interact_menu_fnc_addActionToObject;
*/
//Add TFA OS to ACE/Equipment requiring "tf_rt1523g_black"
 _action = ["open_tfaos", "TFA OS", "a3\ui_f\data\IGUI\RscTitles\RscHvtPhase\JAC_A3_Signal_4_ca.paa", {execVM "TFAOs\TFAOsMain.sqf";}, {(backpack player) in ["tf_rt1523g_black"]}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "ACE_Equipment"], _action] call ace_interact_menu_fnc_addActionToObject;
//LT on respawn
if (isPlayer player && !isServer) then
{

	waituntil {!(IsNull (findDisplay 46))};
	player addEventHandler ["RESPAWN", { [] spawn
			{
			waituntil {!isNull player};
			 [] spawn LT_fnc_mainMenuLoad;
			};
	}];
	[] spawn LT_fnc_mainMenuLoad;

};