Since you have opened this file with the intent to edit it, or at the least see how it works, There are have a few things you need to agree to before editing the contents.

- Be aware that the content inside this mission folder is the work of many modders, scripters, and talented individuals.
- Respect the boundaries of use. All mods/scripts contained are used according to their license and/or fair use terms and are the property of the original author.
- If you modify the contents then PLEASE respect the original authors and give credit to thier work.
- Lastly, all TFA logos, text, filenames, etc. MUST be left in place and cannot be modified with the exception of TFA OS which is an edit of Coala OS found here https://github.com/democore/CoalaOs

A big thank you to all the mods and thier creators that were used in the making of this mission. 

Mods required:
- ACE3
- ALiVE
- CBA_A3
- CUP Terrains - Core
- CUP Terrains - Maps
- Missing Units - Complete package
- Crye Gen 3 Uniforms (NATO Retexture)
- R3F Advanced Logistics [BETA]
- Task Force Radio																					
																					
																					
																						~[TFA]Blunts																					