/*
Call with     nul = [this] execVM "medveh_loadout.sqf"
*/

_veh = _this select 0;
//Clear the current items
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearBackpackCargoGlobal _veh;
//Items
_veh addItemCargoGlobal ["ACE_fieldDressing", 80];
_veh addItemCargoGlobal ["ACE_tourniquet", 20];
_veh addItemCargoGlobal ["ACE_bloodIV", 10];
_veh addItemCargoGlobal ["ACE_bloodIV_250", 10];
_veh addItemCargoGlobal ["ACE_bloodIV_500", 10];
_veh addItemCargoGlobal ["ACE_bodyBag", 20];
_veh addItemCargoGlobal ["ACE_EarPlugs", 20];
_veh addItemCargoGlobal ["ACE_morphine", 20];
_veh addItemCargoGlobal ["ACE_epinephrine", 20];
_veh addItemCargoGlobal ["ALIVE_Tablet", 1];
_veh addItemCargoGlobal ["ACE_key_west", 1];