/*
Call with     nul = [this] execVM "scripts\ammo\vic_loadout.sqf"
*/

_veh = _this select 0;
//Clear the current items
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearBackpackCargoGlobal _veh;
//Weapons
_veh addweaponCargoGlobal ["arifle_MX_Black_Hamr_pointer_F", 2];
_veh addweaponCargoGlobal ["arifle_SPAR_01_blk_F", 2];
//Ammo
_veh addMagazineCargoGlobal ["HandGrenade", 10];
_veh addMagazineCargoGlobal ["30Rnd_65x39_caseless_mag", 10];
_veh addMagazineCargoGlobal ["30Rnd_556x45_Stanag", 10];
//Items
_veh addItemCargoGlobal ["ALIVE_Tablet", 1];	
_veh addItemCargoGlobal ["ACE_fieldDressing", 30];
_veh addItemCargoGlobal ["ACE_CableTie", 30];
_veh addItemCargoGlobal ["ACE_EarPlugs", 4];
_veh addItemCargoGlobal ["ACE_microDAGR", 4];
_veh addItemCargoGlobal ["AdvLog_TowCable", 1];
_veh addItemCargoGlobal ["ACE_key_west", 1];