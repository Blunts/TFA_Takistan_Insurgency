/*
Call with     nul = [this] execVM "hemtt_loadout.sqf"
*/

_veh = _this select 0;
//Clear the current items
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearBackpackCargoGlobal _veh;
//Weapons
_veh addweaponCargoGlobal ["ACE_VMM3", 2];
//Ammo
_veh addMagazineCargoGlobal ["HandGrenade", 10];
_veh addMagazineCargoGlobal ["SatchelCharge_Remote_Mag", 4];
_veh addMagazineCargoGlobal ["DemoCharge_Remote_Mag", 8];
_veh addMagazineCargoGlobal ["ACE_HandFlare_Red", 4];
//Items
_veh addItemCargoGlobal ["ALIVE_Tablet", 1];	
_veh addItemCargoGlobal ["ACE_fieldDressing", 30];
_veh addItemCargoGlobal ["Toolkit", 2];
_veh addItemCargoGlobal ["ACE_wirecutter", 2];
_veh addItemCargoGlobal ["ACE_Clacker", 2];
_veh addItemCargoGlobal ["ACE_DefusalKit", 2];
_veh addItemCargoGlobal ["ACE_key_west", 1];



